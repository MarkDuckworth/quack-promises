Test Readme
-----------
This library uses the Promises/A+ Compliance Test Suite. The file
in this folder ('promises-test-adapter.js') enables the test suite
to run.

See the project readme for instructions to run the tests.