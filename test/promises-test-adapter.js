/**
 * Created by Mark Duckworth on 12/30/2015. Copyright (c) 2015 Mark Duckworth. All rights reserved.
 *
 * This file implements the test adapter for the Promises/A+ compliance tests as specified
 * at https://github.com/promises-aplus/promises-tests
 */

var quack = require("../quack.js");

module.exports.resolved = function(value) {
    var deferred = new quack.Deferred();
    deferred.resolve(value);
    return deferred.promise;
};

module.exports.rejected = function(reason) {
    var deferred = new quack.Deferred();
    deferred.reject(reason);
    return deferred.promise;
};

module.exports.deferred = function() {
    return new quack.Deferred();
};