The "quack" promise library.
============================
The goal of this specification was to see how quickly I could implement the 
[Promises/A+](https://promisesaplus.com/) Spec. The result
 was that I implemented the spec in approximately 5hrs.
 
Documentation
----------------
[API Docs](http://markduckworth.bitbucket.org/quack-promises/)
 
Get It
----------------
Hosted on Bitbucket at [https://bitbucket.org/MarkDuckworth/quack-promises](https://bitbucket.org/MarkDuckworth/quack-promises).

Or run the command: `git clone git@bitbucket.org:MarkDuckworth/quack-promises.git`

Compatability
----------------
* node.js
* web browsers

Tests
----------------
This implementation passes the
 [Promises/A+ Compliance Test Suite](https://github.com/promises-aplus/promises-tests).
 
To run the tests:

1. `npm install`
2. `npm test`

Demo
-----------------
Open the demo.html page in your browser. View the source code for usage.

Build the API Docs
-----------------
To build the API docs to the folder '/docs', run:

1. `npm install`
2. `npm run doc`

Linting
-----------------
This library lints with jshint, using the default settings. To run:

1. `npm install`
2. `npm run lint`