/**
 * Created by Mark Duckworth on 12/30/2015. Copyright (c) 2015 Mark Duckworth. All rights reserved.
 */

(function() {

    /**
     * @namespace quack
     * @description
     * "quack" is a JavaScript Promise library that supports node.js and browsers.
     * It implements the [Promises/A+ Specification](https://promisesaplus.com/).
     */
    var quack = {};

    // Add quack to the browser's global namespace
    if (typeof window !== "undefined") {
        window.quack = quack;
    }

    // Support node.js. Export quack as the module
    // if we determine we are running in context of node.js.
    if (typeof module !== "undefined") {
        module.exports = quack;
    }

    /**
     * Enumeration for promise states. All {@link quack.Promise}
     * objects will be in one of these states.
     * @enum {string}
     * @memberOf quack
     */
    var PromiseState = {
        /** "pending" **/
        pending: "pending",

        /** "fulfilled" **/
        fulfilled: "fulfilled",

        /** "rejected" **/
        rejected: "rejected"
    };

    // Add PromiseState to the public namespace.
    quack.PromiseState = PromiseState;

    /**
     * A helper method to determine if a string is a valid promise state.
     * @param state {string} Determine if this is a valid promise state.
     * @return {boolean} `true` if the string is a valid promise state, otherwise `false`.
     * @memberOf quack.PromiseState
     */
    PromiseState.isValid = function(state) {
        return ((state === PromiseState.pending) ||
        (state === PromiseState.fulfilled) ||
        (state === PromiseState.rejected));
    };

    /**
     * A deferred object is used to provide the eventual result of an operation.
     * Each deferred object has an associated {@link quack.Promise} object, which
     * can be distributed to access the eventual result of the operation. The
     * deferred object itself is used to resolve or reject the promise based on
     * the outcome of the operation.
     * @constructor quack.Deferred
     * @memberOf quack
     * @example
     * function doSomethingAsync() {
     *   // create a deferred object
     *   var deferred = new quack.Deferred();
     *
     *   // Get the promise associated with the deferred.
     *   // The promise will be returned to the caller
     *   // of doSomethingAsync, so the caller can access
     *   // the eventual result of doSomethingAsync
     *   var myPromise = deferred.promise;
     *
     *   // setTimeout is used in place of a meaningful
     *   // operation that will complete sometime in the future
     *   setTimeout(function() {
     *     // resolve the promsie with the value "some value"
     *     deferred.resolve("some value");
     *   }, 1000);
     *
     *   return myPromise;
     * }
     *
     * // Do something asynchronously, and when it is completed
     * // alert the result
     * doSomethingAsync().then(alert);
     */
    quack.Deferred = function Deferred() {

        /**
         * Get the promise associated with this Deferred object.
         * @member quack.Deferred#promise
         * @type {quack.Promise}
         * @example
         * var deferred = new quack.Deferred();
         * var myPromise = deferred.promise;
         */
        this.promise = new Promise();

        /**
         * Resolve this Deferred/Promise with a specified value.
         * @method quack.Deferred#resolve
         * @param value {*} Resolve the Deferred/Promise with this value.
         * @example
         * var deferred = new quack.Deferred();
         * var myPromise = deferred.promise;
         * deferred.resolve("some value");
         */
        this.resolve = function resolve(value) {
            this.promise._internal.resolve(value);
        };

        /**
         * Reject this Deferred/Promise with a specified reason.
         * @method quack.Deferred#reject
         * @param reason {*} Reject the Deferred/Promise with this reason.
         * @example
         * var deferred = new quack.Deferred();
         * var myPromise = deferred.promise;
         * deferred.reject("some reason");
         */
        this.reject = function reject(reason) {
            this.promise._internal.reject(reason);
        };
    };

    /**
     * A promise gives access to the current or eventual result of an operation.
     *
     * A promise should not be created directly, instead create
     * a new {@link quack.Deferred} object and access the promise.
     * @constructor quack.Promise
     * @memberOf quack
     * @example
     * var deferred = new quack.Deferred();
     * var myPromise = deferred.promise;
     */
    function Promise() {
        var _self = this;
        var _value = null;
        var _reason = null;
        var _state = PromiseState.pending;
        var _callbacks = {
            onFulfilled: [],
            onRejected: []
        };

        // Used to prevent a promise from being resolved
        // for a second time, after initially being resolved
        // with another promise.
        var _isTransitioning =  false;

        /**
         * Gets the current state of the promise.
         * @return {string} See possible values in {@link quack.PromiseState}.
         * @method quack.Promise#getState
         * @memberOf quack.Promise
         * @example
         * var deferred = new quack.Deferred();
         * var promise = deferred.promise.
         *
         * var state = promise.getState(); // quack.PromiseState.pending, "pending"
         */
        this.getState = function() {
            return _state;
        };

        /**
         * Callbacks of type 'onFulfilled' are called when a promise is fulfilled.
         * The callback is passed the resolved promise value as the first argument.
         *
         * @callback onFulfilled
         * @param {*} value The resolved value of the promise.
         * @see quack.Promise#then
         */

        /**
         * Callbacks of type 'onRejected' are called when a promise is rejected.
         * The callback is passed the promise's rejection reason as the first argument.
         *
         * @callback onRejected
         * @param {*} reason A value that represents the reason the promise was rejected.
         * @see quack.Promise#then
         */

        /**
         * Provides access to the current or eventual result of the promise.
         * This method is used to provide callbacks to the promise that will
         * be called with the fulfilled value or rejection reason of the promise.
         * This method may be called multiple times per promise, to provide
         * multiple onFulfilled and onReject callbacks per promise. The callbacks
         * will be called in order that they are added using this method.
         *
         * Only callbacks for the eventual state of the promise will be called.
         * For example, if the promise is fulfilled, then only the onFulfilled
         * callbacks will be called; the onRejected callbacks will not be called.
         *
         * Callbacks may be added before or after the promise is resolved. In either
         * case, the callbacks will be executed asynchronously, in the context of
         * a future stack of the javascript runtime.
         *
         * Each call to this method returns a new Promise object, which allows promise
         * chaining. The returned promise object represents the result of the provided
         * callbacks. For example, if the onFulfilled callback is executed and it
         * returns the value "hello", then the promise returned by this method will be
         * resolved with the value "hello".
         * @param [onFulfilled] {onFulfilled} Optional. A callback that will be called
         * if/when the promise is fulfilled.
         * @param [onRejected] {onRejected} Optional. A callback that will be called
         * if/when the promise is rejected.
         * @return {quack.Promise} A new promise that represents the eventual result
         * of the callbacks provided.
         * *If the executed callback returns a value, this
         * promise will be fulfilled with this value.
         * * If the executed callback returns
         * a {@link quack.Promise}, this promise will be resolved with the same value
         * as the returned promise.
         * * If the executed callback throws, this promise will
         * be rejected with the error that was thrown.
         * * Finally, if the executed callback
         * does not return a value, then the returned promise will be resolved with
         * the same value as the promise up the chain.
         * @method quack.Promise#then
         * @memberOf quack.Promise
         * @example
         * var deferred = new quack.Deferred();
         * var promise = deferred.promise;
         *
         * // basic usage - show an alert with the fulfilled value
         * promise.then(alert);
         *
         * // basic usage - show an alert with the rejection reason
         * promise.then(null, alertOnRejected);
         * function alertOnRejected(reason) {
         *   alert("the promise was rejected with the reason: " + reason);
         * }
         *
         * // promise chaining
         * promise
         *   .then(throwingOnFulfilled)
         *   .then(neverCalledOnFulfilled, chainedPromiseOnRejected);
         *
         * function throwingOnFulfilled(value) {
         *   // this function throws
         *   throw new Error("an error just because");
         * }
         *
         * function neverCalledOnFulfilled(value) {
         *   alert("This function should never be called.");
         * }
         *
         * function chainedPromiseOnRejected(reason) {
         *   alert("The chained promise was rejected with reason: " + reason);
         * }
         *
         * // fulfill the original promise
         * deferred.resolve("hello world");
         *
         * // don't forget, it's ok to call `then` even after the promise
         * // has been resolved.
         * setTimeout(function() {
         *   promise.then(fooBarOnFulfilled);
         * }, 1000);
         *
         * function fooBarOnFulfilled(value) {
         *   console.log(value);
         * }
         */
        this.then = function(onFulfilled, onRejected) {
            var _returnValue = new Promise();

            var _onFulfilledWrapper = function(value) {
                if (isFunction(onFulfilled)) {
                    try {
                        var x = onFulfilled(value);
                        _returnValue._internal.resolve(x);
                    } catch (e) {
                        _returnValue._internal.reject(e);
                    }
                } else {
                    _returnValue._internal.resolve(value);
                }
            };

            var _onRejectedWrapper = function(reason) {
                if (isFunction(onRejected)) {
                    try {
                        var x = onRejected(reason);
                        _returnValue._internal.resolve(x);
                    } catch (e) {
                        _returnValue._internal.reject(e);
                    }
                } else {
                    _returnValue._internal.reject(reason);
                }
            };

            _callbacks.onFulfilled.push(_onFulfilledWrapper);
            _callbacks.onRejected.push(_onRejectedWrapper);

            // Attempt to queue up the callbacks for execution, in the case
            // that `then` was called after the promise was resolved.
            _tryQueueCallbacks();

            return _returnValue;
        };

        // the _internal object is used to make members of the Promise
        // object pseudo-internal. This is being used to expose the
        // resolve and reject methods to the Deferred class.
        this._internal = {};

        /**
         * Attempts to resolve this promise with the
         * given value.
         * @param value {*} Resolve ths promise with this value.
         * @private
         */
        this._internal.resolve = (function(value) {
            // If promise and `value` refer to the same object, reject promise
            // with a TypeError as the reason.
            if (value === this) {
                this._internal.reject(new TypeError("Cannot resolve a promise with itself."));
            }

            // Start transitioning the promise state.
            // If we cannot transition this promise, then ignore this call.
            if (!_tryStartTransition()) {
                return;
            }

            _resolve(value);
        }).bind(_self);

        /**
         * Implements the recursive promise resolution procedure as defined
         * in the Promises/A+ specification.
         * @param value {*} Resolve the promise with this value.
         * @private
         */
        function _resolve(value) {
            // If promise and `value` refer to the same object, reject promise
            // with a TypeError as the reason.
            if (value === _self) {
                promise._internal.reject(new TypeError("Cannot resolve a promise with itself."));
            }

            // If `value` is a promise, adopt its state and return.
            if (value instanceof Promise) {
                value.then(_fulfill, _reject);
                return;
            }

            // Determine if the value is then-able.
            var _isThenable = false;

            // The Promises/A+ compliance test forces us
            // to get the value of `then` one time. Although,
            // the spec suggests this approach, there is
            // no known reason to get this, unless the accessor
            // for `then` is not idempotent.
            // Anyway, this forces some strange code, but it's
            // still readable.
            var _then = null;

            // Determine if the object is then-able and get the
            // then method.
            try {
                // there is a strict definition of a "then-able"
                // It is an object, where the typeof evaluates to
                // "function" or "object", and it is not null. The
                // object must also have a property of type "function".
                if (isObject(value) || isFunction(value)) {
                    _then = value.then;
                    _isThenable = isFunction(_then);
                }
            } catch (e) {
                // If retrieving the property `value.then` results
                // in a thrown exception e, reject promise with e as the reason.
                _reject(e);
            }

            // If `value` is then-able, adopt its state
            if (_isThenable) {
                // use a once object to ensure our _resolve
                // and _reject functions are not called multiple times
                // by the then-able.
                var once = new Once();

                try {

                    // We call the `then` method in this way
                    // because the promises/A+ compliance tests
                    // only allow us to access the `then` method
                    // one time. We have already accessed it before,
                    // to validate the type.
                    _then.call(value, once.wrapFunc(_resolve), once.wrapFunc(_reject));
                } catch (e) {
                    once.wrapFunc(_reject)(e);
                }
            }

            // If `value` is not then-able,
            // fulfill promise with `value`
            else {
                _fulfill(value);
            }
        }

        /**
         * Attempt to reject the promise with a given value.
         * @param reason {*} Reject the promsie with this reason.
         * @private
         */
        this._internal.reject = (function(reason) {
            // Start transitioning the promise state.
            // If we cannot transition this promise, then ignore this call.
            if (!_tryStartTransition()) {
                return;
            }

            _reject(reason);
        }).bind(_self);

        // a timeout used to execute promise callbacks asynchronously, on a future stack
        var timeout = null;

        /**
         * Helper method. Attempts to queue the promise's callbacks for
         * execution. It will only queue callbacks for execution if the
         * promise is in a fulfilled or rejected state, therefore it is
         * safe to call this any time new callbacks are added.
         * This will only execute the callbacks that are specified for
         * the final state of the promise.
         * @return {boolean} `true` if callbacks were queued for execution
         * @private
         */
        function _tryQueueCallbacks() {
            if (_state === PromiseState.pending) {
                // we will not call any callbacks
                // until after the Promise has finished
                // transitions from the pending state
                return false;
            }

            if (timeout) {
                // we will not re-queue callbacks
                // if they are already queued
                return false;
            }

            timeout = setTimeout(_queueCallbacks, 0);

            function _queueCallbacks() {
                var callbacks;
                var callbackArg;
                var callback;

                // Get the array of (onFulfilled or onRejected) callbacks
                // that will be called for the state of this promise. Also,
                // get the argument that we will pass when calling the callback.
                // Also, free references to other callbacks that we will never
                // call.
                if (_state === PromiseState.fulfilled) {
                    callbacks = _callbacks.onFulfilled;
                    callbackArg = _value;

                    // clear the array of onRejected callbacks
                    // so we do not hold them in memory.
                    _callbacks.onRejected.length = 0;
                } else {
                    callbacks = _callbacks.onRejected;
                    callbackArg = _reason;

                    // clear the array of onFulfilled callbacks
                    // so we do not hold them in memory.
                    _callbacks.onFulfilled.length = 0;
                }

                // call each callback in the order that is was
                // added.
                while (callback = callbacks.shift()) { // jshint ignore:line
                    callback(callbackArg);
                }

                timeout = null;
            }

            return true;
        }

        /**
         * Helper method to move the promise into a fulfilled state
         * @param value {*} The value to fulfill the promise with.
         * @private
         */
        function _fulfill(value) {
            if (!_tryFinishTransition(PromiseState.fulfilled)) {
                return;
            }

            // save the value
            _value = value;

            // queue up the callbacks, so they
            // are called with the JS VM stack is empty
            _tryQueueCallbacks();
        }

        /**
         * Helper method to move the promise into a rejected state.
         * @param reason {*} The reason to reject the promise with.
         * @private
         */
        function _reject(reason) {
            if (!_tryFinishTransition(PromiseState.rejected)) {
                return;
            }

            // save the reason
            _reason = reason;

            // queue up the callbacks, so they
            // are called with the JS VM stack is empty
            _tryQueueCallbacks();
        }

        /**
         * Helper method to determine if the promise can start
         * the transition from the pending state to a resolved state.
         * If the promise can be transitioned, it moves the promise
         * into a transitioning state (publically it still appears as
         * pending). If the promise cannot begin transition, this returns `false`.
         * @return {boolean} `false` if the promise is already transitioning
         * or is resolved, otherwise `true`.
         * @private
         */
        function _tryStartTransition() {
            if (_isTransitioning || (_state !== PromiseState.pending)) {
                // we cannot start transitioning
                return false;
            }

            // we can start transitioning
            _isTransitioning = true;
            return true;
        }

        /**
         * Helper method to complete state transitioning.
         * This will move the promise into a resolved state,
         * either "fulfilled" or "rejected" and move the promise
         * out of a transitioning state.
         * This method will only complete successfully if the promise
         * was already in a transitioning state.
         * @param newState {string} see {@link quack.PromiseState}
         * @return {boolean} `true` if successful, otherwise `false`
         * @private
         */
        function _tryFinishTransition(newState) {
            if (!_isTransitioning ||
                (_state !== PromiseState.pending) ||
                !PromiseState.isValid(newState)) {

                // we cannot finish the state transition
                return false;
            }

            _state = newState;
            _isTransitioning = false;

            return true;
        }
    }
    quack.Promise = Promise;

    /**
     * Helper function to determine if a value is a function.
     * @param value {*} Determine if this value is a function.
     * @return {boolean} `true` if value is a function
     * @private
     */
    function isFunction(value) {
        return typeof(value) === "function";
    }

    /**
     * Helper function to determine if a value is an object, by
     * definition of the Promises/A+ specification. That is,
     * calling `typeof value === 'object'` and the value is not null.
     * @param value {*} The value to determine if it is an object
     * by definition of the Promises/A+ spec.
     * @return {boolean} `true` if it is an object by definition.
     * @private
     */
    function isObject(value) {
        return (value !== null) && (typeof value === "object");
    }

    /**
     * Helper class to ensure a function from a group of functions
     * is called only one time.
     * @constructor
     * @private
     */
    function Once() {
        var isCalled = false;

        /**
         * Add a function to this once and return a wrapped function.
         * The outer function (wrapper) enforces limitations on calling.
         * @param myFunction {Function} The function you want to add to the once group.
         * @return {Function} The function you should call.
         */
        this.wrapFunc = function(myFunction) {
            return function() {
                if (isCalled) {
                    return;
                }

                isCalled = true;
                myFunction.apply(null, arguments);
            };
        };
    }

    /**
     * Poly-fill Function.prototype.bind for older browsers.
     */
    if (!Function.prototype.bind) {
        Function.prototype.bind = function(oThis) {
            if (typeof this !== 'function') {
                // closest thing possible to the ECMAScript 5
                // internal IsCallable function
                throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
            }

            var aArgs   = Array.prototype.slice.call(arguments, 1),
                fToBind = this,
                fNOP    = function() {},
                fBound  = function() {
                    return fToBind.apply(this instanceof fNOP ?
                            this :
                            oThis,
                        aArgs.concat(Array.prototype.slice.call(arguments)));
                };

            if (this.prototype) {
                // native functions don't have a prototype
                fNOP.prototype = this.prototype;
            }
            fBound.prototype = new fNOP();

            return fBound;
        };
    }
})();